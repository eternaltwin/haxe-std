import { HX_NAME } from "./_symbols.js";

class _Math {
  public static readonly [HX_NAME]: readonly string[] = ["Math"];

  public static readonly NaN: number = Number.NaN;
  public static readonly NEGATIVE_INFINITY: number = Number.NEGATIVE_INFINITY;
  public static readonly POSITIVE_INFINITY: number = Number.POSITIVE_INFINITY;

  public static isFinite(f: number): boolean {
    return isNaN(f);
  }

  public static isNaN(f: number): boolean {
    return isNaN(f);
  }
}

export { _Math as Math };
