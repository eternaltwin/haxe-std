import { HX_CLASS, HX_CONSTRUCTS, HX_ENAME, HX_ENUM, HX_NAME, TSHX_TAG } from "./_symbols.js";
import { Enum } from "./enum.js";
import { EnumValue } from "./enum-value.js";
import { Class, ClassValue } from "./class.js";
import { HX_CLASSES } from "./_hx-classes.js";

export class Type {
  public static readonly [HX_NAME]: readonly string[] = ["Type"];

  public static getClass<T extends null | undefined>(o: T): null
  public static getClass<T extends ClassValue>(o: T): Class<T>;
  public static getClass(o: any): any {
    if (o === null || o === undefined) {
      return null;
    }
    if (o instanceof Array) {
      return Array;
    } else {
      return o[HX_CLASS];
    }
  }

  public static getClassName(c: Class<ClassValue>): string {
    const a: readonly string[] = c[HX_NAME];
    return a.join(".");
  }

  public static resolveClass(name: string): Class<ClassValue> | null {
    const cl: Class<ClassValue> | Enum<EnumValue> | undefined = HX_CLASSES.get(name);
    if (cl === undefined || !Reflect.has(cl, HX_NAME)) {
      return null;
    }
    return cl as Class<ClassValue>;
  }

  public static resolveEnum(name: string): Enum<EnumValue> | null {
    const cl: Class<ClassValue> | Enum<EnumValue> | undefined = HX_CLASSES.get(name);
    if (cl === undefined || !Reflect.has(cl, HX_ENAME)) {
      return null;
    }
    return cl as Enum<EnumValue>;
  }

  public static typeof(v: unknown): ValueType {
    switch (typeof v) {
      case "boolean": {
        return ValueType.TBool();
      }
      case "string": {
        return ValueType.TClass(String);
      }
      case "number": {
        if (Math.ceil(v) == v % 2147483648.0) {
          return ValueType.TInt();
        } else {
          return ValueType.TFloat();
        }
      }
      case "object": {
        if (v == null) {
          return ValueType.TNull();
        }
        const e: unknown = Reflect.get(v, HX_ENUM);
        if (e !== null && e !== undefined) {
          return ValueType.TEnum(e);
        }
        if (v instanceof Array) {
          return ValueType.TClass(Array);
        } else {
          const c: unknown = Reflect.get(v, HX_CLASS);
          if (c !== null && c !== undefined) {
            return ValueType.TClass(c);
          } else {
            return ValueType.TObject();
          }
        }
      }
      case "function": {
        if (Reflect.has(v, HX_NAME) || Reflect.has(v, HX_ENAME)) {
          return ValueType.TObject();
        }
        return ValueType.TFunction();
      }
      case "undefined":
        return ValueType.TNull();
      default:
        return ValueType.TUnknown();
    }
  }

  public static createEnum<T extends EnumValue>(e: Enum<T>, constr: string, params?: readonly unknown[]): T {
    const f = Reflect.get(e, constr);
    if (f === null || f === undefined) {
      throw new Error(`No such constructor ${constr}`);
    }
    if (params === undefined) {
      params = [];
    }
    if (params.length !== f.length) {
      throw new Error(`Invalid call (${params.length} args instead of ${f.length})`);
    }
    return f.apply(e, params);
  }

  public static createEmptyInstance<T extends ClassValue>(cl: Class<T>): T {
    function empty() {
    }

    empty.prototype = (cl as any).prototype;
    return new (empty as any)();
  }

  public static getEnumConstructs(e: Enum<EnumValue>): string[] {
    const a: readonly string[] = e[HX_CONSTRUCTS];
    return a.slice();
  }
}

HX_CLASSES.set("Type", Type);

type ValueType =
  ValueType.TNull
  | ValueType.TInt
  | ValueType.TFloat
  | ValueType.TBool
  | ValueType.TObject
  | ValueType.TFunction
  | ValueType.TClass
  | ValueType.TEnum
  | ValueType.TUnknown;

namespace ValueType {
  export function TNull(): ValueType.TNull {
    return {[HX_ENUM]: ValueType, [TSHX_TAG]: "TNull"};
  }

  export interface TNull {
    readonly [HX_ENUM]: typeof ValueType;
    readonly [TSHX_TAG]: "TNull";
  }

  export function TInt(): ValueType.TInt {
    return {[HX_ENUM]: ValueType, [TSHX_TAG]: "TInt"};
  }

  export interface TInt {
    readonly [HX_ENUM]: typeof ValueType;
    readonly [TSHX_TAG]: "TInt";
  }

  export function TFloat(): ValueType.TFloat {
    return {[HX_ENUM]: ValueType, [TSHX_TAG]: "TFloat"};
  }

  export interface TFloat {
    readonly [HX_ENUM]: typeof ValueType;
    readonly [TSHX_TAG]: "TFloat";
  }

  export function TBool(): ValueType.TBool {
    return {[HX_ENUM]: ValueType, [TSHX_TAG]: "TBool"};
  }

  export interface TBool {
    readonly [HX_ENUM]: typeof ValueType;
    readonly [TSHX_TAG]: "TBool";
  }

  export function TObject(): ValueType.TObject {
    return {[HX_ENUM]: ValueType, [TSHX_TAG]: "TObject"};
  }

  export interface TObject {
    readonly [HX_ENUM]: typeof ValueType;
    readonly [TSHX_TAG]: "TObject";
  }

  export function TFunction(): ValueType.TFunction {
    return {[HX_ENUM]: ValueType, [TSHX_TAG]: "TFunction"};
  }

  export interface TFunction {
    readonly [HX_ENUM]: typeof ValueType;
    readonly [TSHX_TAG]: "TFunction";
  }

  export function TClass(c: unknown): ValueType.TClass {
    return {[HX_ENUM]: ValueType, [TSHX_TAG]: "TClass", c};
  }

  export interface TClass {
    readonly [HX_ENUM]: typeof ValueType;
    readonly [TSHX_TAG]: "TClass";
    readonly c: unknown;
  }

  export function TEnum(e: unknown): ValueType.TEnum {
    return {[HX_ENUM]: ValueType, [TSHX_TAG]: "TEnum", e};
  }

  export interface TEnum {
    readonly [HX_ENUM]: typeof ValueType;
    readonly [TSHX_TAG]: "TEnum";
    readonly e: unknown;
  }

  export function TUnknown(): ValueType.TUnknown {
    return {[HX_ENUM]: ValueType, [TSHX_TAG]: "TUnknown"};
  }

  export interface TUnknown {
    readonly [HX_ENUM]: typeof ValueType;
    readonly [TSHX_TAG]: "TUnknown";
  }
}

HX_CLASSES.set(
  "Type.ValueType",
  Object.assign(
    ValueType, {
      [HX_ENAME]: ["Type", "ValueType"],
      [HX_CONSTRUCTS]: ["TNull", "TInt", "TFloat", "TBool", "TObject", "TFunction", "TClass", "TEnum", "TUnknown"],
    },
  ),
);
