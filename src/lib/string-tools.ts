import { HX_NAME } from "./_symbols.js";

export class StringTools {
  public static readonly [HX_NAME]: readonly string[] = ["StringTools"];

  public static urlEncode(s: string): string {
    return encodeURIComponent(s);
  }

  public static urlDecode(s: string): string {
    return decodeURIComponent(s);
  }

  public static fastCodeAt(s: string, index: number): number {
    return s.charCodeAt(index);
  }

  /**
   * Tells if `c` represents the end-of-file (EOF) character.
   */
  public static isEof(c: number): boolean {
    return c !== c; // fast NaN
  }
}
