import { HX_NAME } from "./_symbols.js";
import { Boot } from "./js/boot.js";

export class Std {
  public static readonly [HX_NAME]: readonly string[] = ["Std"];

  public static string(s: unknown): string {
    return Boot.__string_rec(s, "");
  }

  public static parseFloat(x: string): number {
    return parseFloat(x);
  }

  public static is(v: unknown, t: unknown): boolean {
    return Boot.__instanceof(v, t);
  }
}
