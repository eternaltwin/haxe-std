import { HX_CLASS, HX_NAME } from "./_symbols.js";

export interface ClassValue {
  [HX_CLASS]: Class<this>;
}

export interface Class<T extends ClassValue> {
  [HX_NAME]: readonly string[];
}

export const Class = {
  [HX_NAME]: ["Class"],
};
