/**
 * Array representing the full path for a class.
 *
 * For example, the class `foo.bar.Baz` must have the value `["foo", "bar", "Baz"]`
 */
export const HX_NAME: unique symbol = Symbol("__name__");

/**
 * Array representing the full path for an enum class.
 *
 * For example, the enum class `foo.bar.Baz` must have the value `["foo", "bar", "Baz"]`
 */
export const HX_ENAME: unique symbol = Symbol("__ename__");

/**
 * Array of enum variant names (strings) on a Haxe enum class.
 */
export const HX_CONSTRUCTS: unique symbol = Symbol("__constructs__");

/**
 * Reference to the corresponding enum class on an enum instance.
 */
export const HX_ENUM: unique symbol = Symbol("__enum__");

/**
 * Reference to the corresponding class on a class instance.
 */
export const HX_CLASS: unique symbol = Symbol("__class__");

/**
 * Variant tag (string) on an enum instance.
 */
export const TSHX_TAG: unique symbol = Symbol("__tag__");
