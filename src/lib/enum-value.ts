import { HX_ENUM, TSHX_TAG } from "./_symbols.js";
import { Enum } from "./enum";

export interface EnumValue {
  [HX_ENUM]: Enum<this>;
  [TSHX_TAG]: string;
}
