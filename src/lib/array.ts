import { HX_NAME } from "./_symbols.js";

const _Array = Object.assign(Array, {[HX_NAME]: ["Array"]});
export { _Array as Array };
