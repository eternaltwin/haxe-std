import { HX_NAME } from "./_symbols.js";

export class StringBuf {
  public static readonly [HX_NAME]: readonly string[] = ["StringBuf"];

  private b: string;

  public constructor() {
    this.b = "";
  }

  public get length(): number {
    return this.b.length;
  }

  public add(x: unknown): void {
    this.b += x;
  }

  public addChar(c: number): void {
    this.b += String.fromCharCode(c);
  }

  public toString(): string {
    return this.b;
  }
}
