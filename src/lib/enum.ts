import { HX_CONSTRUCTS, HX_ENAME, HX_NAME, TSHX_TAG } from "./_symbols.js";
import { EnumValue } from "./enum-value";

export interface Enum<T extends EnumValue> {
  [HX_ENAME]: readonly string[];
  [HX_CONSTRUCTS]: readonly (T[typeof TSHX_TAG])[];
}

export const Enum = {
  [HX_NAME]: ["Enum"],
};
