import { HX_NAME } from "./_symbols.js";

class _Reflect {
  public static readonly [HX_NAME]: readonly string[] = ["Reflect"];

  public static fields(o: unknown): string[] {
    if (typeof o !== "object") {
      return [];
    } else if (o === null) {
      return [];
    }
    const a: string[] = [];
    for (const f in o) {
      if (f != "__id__" && f != "hx__closures__" && Object.prototype.hasOwnProperty.call(o, f)) {
        a.push(f);
      }
    }
    return a;
  }

  public static field(o: unknown, field: string): unknown | null {
    if (typeof o !== "object") {
      return null;
    } else if (o === null) {
      return null;
    }
    try {
      return Reflect.has(o, field) ? Reflect.get(o, field) : null;
    } catch (e) {
      return null;
    }
  }

  public static setField(o: object, field: string, value: unknown): void {
    Reflect.set(o, field, value);
  }
}

export { _Reflect as Reflect };
