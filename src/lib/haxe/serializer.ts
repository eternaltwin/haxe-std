import { StringBuf } from "../string-buf.js";
import { StringMap } from "./ds/string-map.js";
import { StringTools } from "../string-tools.js";
import { Reflect } from "../reflect.js";
import { Math } from "../math.js";
import { Type } from "../type.js";
import { Std } from "../std.js";
import { HX_NAME, TSHX_TAG } from "../_symbols.js";

export class Serializer {
  public static readonly [HX_NAME]: readonly string[] = ["haxe", "Serializer"];

  /**
   * We mark this variable as a readonly to avoid actions at distance.
   * Use the instance member `useCache``if you need to configure it.
   */
  public static readonly USE_CACHE: boolean = false;

  /**
   * We mark this variable as a readonly to avoid actions at distance.
   * Use the instance member `useEnumIndex``if you need to configure it.
   */
  public static readonly USE_ENUM_INDEX: boolean = false;

  private static readonly BASE64: string = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789%:";

  private readonly buf: StringBuf;
  private readonly cache: unknown[];
  /**
   * String hash
   */
  private readonly shash: StringMap<number>;
  private scount: number;
  public useCache: boolean;
  public useEnumIndex: boolean;

  public constructor() {
    this.buf = new StringBuf();
    this.cache = [];
    this.useCache = Serializer.USE_CACHE;
    this.useEnumIndex = Serializer.USE_ENUM_INDEX;
    this.shash = new StringMap();
    this.scount = 0;
  }

  public toString(): string {
    return this.buf.toString();
  }

  private serializeString(s: string): void {
    const x: number | null = this.shash.get(s);
    if (x !== null) {
      this.buf.add("R");
      this.buf.add(x);
      return;
    }
    this.shash.set(s, this.scount++);
    this.buf.add("y");
    s = StringTools.urlEncode(s);
    this.buf.add(s.length);
    this.buf.add(":");
    this.buf.add(s);
  }

  private serializeRef(v: unknown): boolean {
    for (let i: number = 0; i < this.cache.length; i++) {
      if (this.cache[i] == v) {
        this.buf.add("r");
        this.buf.add(i);
        return true;
      }
    }
    this.cache.push(v);
    return false;
  }

  private serializeFields(v: unknown): void {
    for (const f in Reflect.fields(v)) {
      this.serializeString(f);
      this.serialize(Reflect.field(v, f));
    }
    this.buf.add("g");
  }

  public serialize(v: unknown) {
    const type = Type.typeof(v);
    switch (type[TSHX_TAG]) {
      case "TNull": {
        this.buf.add("n");
        break;
      }
      case "TInt": {
        const vInt: number = v as number;
        if (vInt === 0) {
          this.buf.add("z");
          return;
        }
        this.buf.add("i");
        this.buf.add(v);
        break;
      }
      case "TFloat": {
        const vFloat: number = v as number;
        if (Math.isNaN(vFloat)) {
          this.buf.add("k");
        } else if (!Math.isFinite(vFloat)) {
          this.buf.add(vFloat < 0 ? "m" : "p");
        } else {
          this.buf.add("d");
          this.buf.add(vFloat);
        }
        break;
      }
      case "TBool": {
        const vBool: boolean = v as boolean;
        this.buf.add(vBool ? "t" : "f");
        break;
      }
      case "TClass": {
        throw new Error("NotImplemented: Serializer#serialize|ValueTypeTag.TClass");
      }
      case "TObject": {
        throw new Error("NotImplemented: Serializer#serialize|ValueTypeTag.TObject");
      }
      case "TEnum": {
        throw new Error("NotImplemented: Serializer#serialize|ValueTypeTag.TEnum");
      }
      case "TFunction": {
        throw new Error("AssertionError: Cannot serialize function");
      }
      default: {
        throw new Error("AssertionError: Cannot serialize " + Std.string(v));
      }
    }
  }

  public serializeException(e: unknown) {
    this.buf.add("x");
    this.serialize(e);
  }

  public static run(v: unknown): string {
    const s = new Serializer();
    s.serialize(v);
    return s.toString();
  }
}
